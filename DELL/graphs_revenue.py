# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 16:07:26 2019

@author: Saransh
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dataset=pd.read_csv("C://Users//Saransh//Desktop//Dell//dataset_customers (Autosaved).csv")
products=["Data Protection","Hardisk","Laptop","Networking","Server","Software","Storage"]



product="Laptop"
    
dataset2=dataset.loc[(dataset['Category'] ==product)]


price=dataset2.sort_values("Service Purchased")["Price of Service"].unique()
price=price[:-1]
l=[]
l=dataset2.sort_values("Service Purchased")["Service Purchased"].unique().tolist()
l=l[:-1]
count=dataset2.groupby('Service Purchased',as_index=False).count()

count=count.rename(columns = {'Customer Id':'Count'})
count=count[['Service Purchased','Count']]
c=list(count['Count'])
revenue=c*price


##plotting bar graphs Revenue and Service Purchased
labels=l
index=np.arange(len(labels))
barlist=plt.bar(index,revenue)
plt.xlabel('Service')
plt.ylabel('Price in Dollars')
plt.xticks(index, labels)
plt.title("Revenue earned from services offered for "+product+" since 2014-2018")
plt.show()
