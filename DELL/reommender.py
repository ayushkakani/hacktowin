# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 11:27:55 2019

@author: Ayush
"""

##reommendations based upon the servies generally purchased when an issue is encountered

import numpy as np
import random
import pandas as pd


def recoomender(sel_produt,sel_issue):
    dataset=pd.read_csv("C://Users//Saransh//Desktop//Dell//dataset_customers_issues _recommender.csv")
    products=["Data Protection","Hardisk","Laptop","Networking","Server","Software","Storage"]
    
    ## Provided by User
    sel_product="Data Protection"
    sel_issue="Sensitive Data Protection"
    
    
    
    services=(dataset['Service Purchased'].unique())
    issues=(dataset['Issue'].unique())
    
    
    
    service_count=dataset.groupby("Service Purchased",as_index=False).count()
    service_count=service_count.rename(columns={"Customer Id":"Count"})
    service_count=service_count[["Service Purchased","Count"]]
    ## service cout is aphabetical
    
    
    unique_services=[]
    temp_unique=[]
    i=0
    for i in range(0,len(issues)):
        temp=issues[i]
        for j in range(0,len(dataset)):
            if dataset['Service Purchased'][j] not in temp_unique and dataset['Issue'][j]==temp and dataset['Service Purchased'][j] in services:
                temp_unique.append(dataset['Service Purchased'][j])
        unique_services.append([temp,temp_unique])
        temp_unique=[]
        
        
    
    product_services=[]
    for product in products:
        df4=dataset.loc[(dataset['Category'] ==product)]
        df4.groupby("Service Purchased",as_index=False)
        df4=df4.sort_values("Service Purchased")["Service Purchased"].unique().tolist()
        df4=df4[:-1] 
        product_services.append(df4)
        
        
    
    
    for item in unique_services:
        if(item[0] == sel_issue):
            matched=item[1]
    
    dict_services={}        
    for item in matched:
        item_count=service_count.loc[(service_count["Service Purchased"]==item)]
        item_count=item_count["Count"]
        item_count+=500
        item_count=int(item_count/846*100)
        dict_services.update([(item, item_count)])
    
    #c=0 
    #for l in product_services:
    #    c+=1
    #    for s in l:
    #        if(s==sel_issue):
    #            break
        
                
    index=products.index(sel_product)
    services_products=product_services[index]
    uncommon=list(set(services_products)-set(matched))
    
    recommend=random.randint(1,len(uncommon)-1)
    
    uncommon_list=[]
    for number in range(recommend):
        i=random.randint(0,recommend-1)
        uncommon_list.append(uncommon[i])
    uncommon_list=list(set(uncommon_list))
    for item in uncommon_list:
        item_count=service_count.loc[(service_count["Service Purchased"]==item)]
        item_count=item_count["Count"]
        item_count+=200
        item_count=int(item_count/846*100)
        dict_services.update([(item, item_count)])
    return dict_services
    