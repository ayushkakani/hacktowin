# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 07:30:07 2019

@author: Saransh
"""


# recommendatin generated based on earlier customer purchases
import numpy as np
import random
import pandas as pd

dataset=pd.read_csv("dataset_customers_complete_issues.csv")

services=(dataset['Service Purchased'].unique())

issues=(dataset['Issue'].unique())

unique_services=[]
temp_unique=[]
i=0
for i in range(0,len(issues)):
    temp=issues[i]
    for j in range(0,len(dataset)):
        if dataset['Service Purchased'][j] not in temp_unique and dataset['Issue'][j]==temp and dataset['Service Purchased'][j] in services:
            temp_unique.append(dataset['Service Purchased'][j])
    unique_services.append([temp,temp_unique])
    temp_unique=[]
    

l=dataset.groupby('Service Purchased',as_index=False).count()
s_n=l['Service Purchased']
s_f=l['Location']

nan_l=[]
x=np.asarray((dataset['Service Purchased']))

for i in range(0,len(x)):
    if x[i] not in services:
        nan_l.append(i)
        

final=[]
i=0
for i in range(0,len(dataset)):
    if i not in nan_l:
        temp=dataset['Issue'][i]
        for j in range(0,len(unique_services)):
            if unique_services[j][0] == temp:
                tls=len(unique_services[j][1])
                nls=random.randint(1,tls)
                temp=[]
                c=0
                while c<nls:
                    print(i,j)
                    r=random.randint(0,tls-1)
                    print('total services',tls)
                    print('number of services recommended',nls)
                    print('index',r)
                    if unique_services[j][1][r] not in temp:
                        print('found',unique_services[j][1][r])
                        print('pre',temp)
                        temp.append(unique_services[j][1][r])
                        c+=1
        final.append(temp)
        temp=[]
    else:
        print('None',i)
        final.append('None')

finalx=np.asarray(final)

d_f=[]
for i in range(0,len(dataset)):
    d_f.append([dataset['Issue'][i], final[i]])

max_l=0
for i in range(0,len(final)):
    if len(final[i])>max_l:
        max_l=len(final[i])

for i in range(0,len(final)):
    if len(final[i])<max_l:
        for j in range(len(final[i]),max_l):
            finalx[i].append('nan')

finalx_l=finalx.tolist()

finalx_l=[x for x in finalx_l if x!='None']
from apyori import apriori
rules = apriori(finalx_l, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2)

# Visualising the results
results = list(rules)


j=43
print("items are:",results[j][0])#items
print("aggregate support",results[j][1])#support
for i in range(0,len(results[j])-1):
    print("item given",results[j][2][i][0])#item given
    print("item recommended",results[j][2][i][1])#item recommended
    print("item confidence",results[j][2][i][2])#item confidence 
    print("item lift",results[j][2][i][3])#item lift 

