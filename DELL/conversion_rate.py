# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 20:45:29 2019

@author: Saransh
"""
## Analysing the conversion rate


import numpy as np
import random
import pandas as pd
from sklearn.linear_model import LinearRegression
import numpy as np
import random

dataset=pd.read_csv("C://Users//Saransh//Desktop//Dell//dataset_customers_issues _recommender.csv")
products=["Data Protection","Hardisk","Laptop","Networking","Server","Software","Storage"]
years=[2014,2015,2016,2017,2018]
X=np.asarray(years)
X=X.reshape(-1, 1)
regressor = LinearRegression()
array=np.zeros((7,6),dtype=float)

i=0
for product in products:
    dataset2=dataset.loc[(dataset['Category'] ==product)]
    
    null_year_wise=[]
    for year in years:
        df2=dataset2.loc[(dataset2["Year of Purchase"]==year)]
        no_rows=df2.shape[0]
        df3=dataset2.loc[(dataset2["Year of Purchase"]==year)].count()
        not_null=int(df3["Service Purchased"])
        null_year_wise.append((not_null/no_rows))
        
    y=np.asarray(null_year_wise)
    regressor.fit(X, y)  ## aplyying linear regresssor to predict outcomes for 2019 year
    X_test=np.array([[2019]])# to create two 2d array
    y_pred = regressor.predict(X_test)
    y_pred=np.asscalar(y_pred)
    null_year_wise.append(y_pred)
    array[i]=null_year_wise
    i+=1
    
        
