# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 03:00:14 2019

@author: Saransh
"""


##plotting pie charts for services purchased for a particular product
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dataset=pd.read_csv("C://Users//Saransh//Desktop//Dell//dataset_customers (Autosaved).csv")
products=["Data Protection","Hardisk","Laptop","Networking","Server","Software","Storage"]


product="Software"
#for product in products:
dataset2=dataset.loc[(dataset['Category'] ==product)]
totalentries=dataset2.shape[0]
dataset2["Service Purchased"].fillna("None", inplace=True)
l=[]
l=dataset2["Service Purchased"].unique().tolist()
l.sort()
count=dataset2.groupby('Service Purchased',as_index=False).count()

count=count.rename(columns = {'Customer Id':'Count'})
count=count[['Service Purchased','Count']]
c=list(count['Count'])

plt.pie(c, labels=l, startangle=90, autopct='%.1f%%')
plt.title("Services Purchased for "+product+" for year 2014-2018")
plt.show()
