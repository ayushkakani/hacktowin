# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 00:52:54 2019

@author: Saransh
"""
## regressor model tp predict revenue for the coming year 2019

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

dataset=pd.read_csv("C://Users//Saransh//Desktop//Dell//dataset_customers (Autosaved).csv")

product=["Laptop","Hardisk","Server","Data Protection","Networking","Software","Storage"]
service=["Premium Support","EMC ProSupport for Software","Legacy Server Models","Data Backup & Protection Software","Network Management Software","Visualization Solution","Entry Level Mid Range"]
years=[2014,2015,2016,2017,2018]
i=0
array=np.zeros((7,6),dtype=int)
#products="Laptop"
for products in product:
    y_list=[]
    dataset2=dataset.loc[(dataset['Category'] == products)]
    dataset2=dataset2.loc[(dataset2["Service Purchased"]==service[i])]
    dataset3=dataset2.groupby("Year of Purchase",as_index=False).count()
    dataset3=dataset3.rename(columns = {'Customer Id':'Units Sold'})
    dataset3=dataset3[['Year of Purchase','Units Sold']]
    

    X = dataset3.iloc[:, :-1].values
    y = dataset3.iloc[:, 1].values
    c=0
    for year in years:
        if(year in X):
            c+=1
        else:
            print(year)
            X=np.insert(X,c,[year])
            y=np.insert(y,c,[0])
            c+=1
    y_list=list(y)
    X=X.reshape(-1, 1)
    y=y.reshape(-1, 1)
    
    regressor = LinearRegression()
    regressor.fit(X, y)
    
    X_test=np.array([[2019]])# to create two 2d array
    y_pred = regressor.predict(X_test)
    y_pred=round(np.asscalar(y_pred))
    y_list.append(y_pred)
    array[i]=y_list
    i+=1


